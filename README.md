# Manajemen Data dan Informasi Bisnis Sewa Zoom

Repository ini berisikan berkas-berkas yang diperlukan dalam manajemen data dan informasi untuk bisnis sewa zoom.

## Hasil analisis :

1. Pihak yang terlibat di dalamnya:
    1) Supplier (Zoom)
    2) Pemilik bisnis
    3) Social media admin
    4) Customer Service
    5) User / costumer

2. Aktivitas yang terjadi dalam bisnis sewa zoom:

    a. Aktivitas Pemilik :
    1) Pemilik bisnis melakukan pembelian akun premium dari supplier (Zoom)
    2) Pemilik bisnis melakukan pendataan akun
    3) Pemilik bisnis menentukan paket yang ditawarkan dan harga (per bulan,
    perhari, per jam)
    4) Membuka penyewaan
    5) Social media admin melakukan promosi dan publikasi
    6) Customer service mendata pesanan user
    7) Customer service melakukan pengecekan / verifikasi
    8) Setelah dicek, user akan diberikan akun zoom premium

    b. Aktivitas Penyewa :
    1) User mendapatkan info produk melalui sosial media / website
    2) User melakukan pemesanan melalui DM / website
    3) User melakukan transfer biaya pembelian
    4) Setelah dicek oleh customer service, user akan mendapatkan akun  zoom 
    premium

3. Aturan yang terdapat di dalamnya :
    1) Satu orang hanya boleh menyewa maks. 2 akun
    2) Tarif sudah tetap tidak dapat di nego
    3) Terdapat bonus fitur tambahan yang didapatkan jika telah melakukan 2 
    kali pemesanan
    4) Pembayaran via transfer bank, uang elektronik (Gopay, OVO, LinkAja) 
    dan melalui minimarket (Alfamart, Indomaret)
    5) Batas pembayaran setelah transaksi maks. 2x24 jam

## Manajemen data dan informasi

Urutan manajemen berdasar hasil analisis yaitu:
1. Membuat ERD
2. Membuat tabel database
3. Membuat tabel BPMN
4. Membuat tabel hubungan antara proses bisnis dengan master data
5. Membuat matriks proses terhadap entitas data
6. Membuat tabel arsitektur sistem informasi
7. Membuat tabel arsitektur aplikasi
8. Membuat portofolio aplikasi
